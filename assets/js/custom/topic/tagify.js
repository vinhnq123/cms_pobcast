// The DOM elements you wish to replace with Tagify
var input = document.querySelector("#kt_tagify_users");


// Initialize Tagify script on the above inputs
new Tagify(input, {
    whitelist: ["Host 1", "Host 2", "Host 3", "Host 4", "Host 5", "Host 6", "Host 7", "Host 8"],
    maxTags: 10,
    dropdown: {
        maxItems: 20,           // <- mixumum allowed rendered suggestions
        classname: "tagify__inline__suggestions", // <- custom classname for this dropdown, so it could be targeted
        enabled: 0,             // <- show suggestions on focus
        closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
    }
});

var input2 = document.querySelector("#kt_tagify_4");
new Tagify(input2);