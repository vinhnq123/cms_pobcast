

$("#kt_table_users").DataTable({
    "language": {
        
        "zeroRecords": "Không tìm thấy",
        "info": "Hiển thị _PAGE_ of _PAGES_",
        "infoEmpty": "Không có dữ liệu",
        "infoFiltered": "(Trong _MAX_ dữ liệu)"
    },
    "dom":
     "<'row'" + 
     "<'col-sm-4'f>" +
     ">" +
   
     "<'table-responsive'tr>" +
     "<'row'" +
     "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
     "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
     ">"
   });