"use strict";
var KTUsersAddUser = (function () {
    const t = document.getElementById("kt_modal_add_topic"),
        e = t.querySelector("#kt_modal_add_topic_form"),
        n = new bootstrap.Modal(t);
    return {
        init: function () {
            (() => {
                var o = FormValidation.formValidation(e, {
                    fields: { audio_name: { validators: { notEmpty: { message: "Vui lòng nhập tên" } } }, audio_link: { validators: { notEmpty: { message: "Vui lòng nhập link" } } } },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                });
                const i = t.querySelector('[data-kt-topic-modal-action="submit"]');
                i.addEventListener("click", (t) => {
                    t.preventDefault(),
                        o &&
                            o.validate().then(function (t) {
                                console.log("validated!"),
                                    "Valid" == t
                                        ? (i.setAttribute("data-kt-indicator", "on"),
                                          (i.disabled = !0),
                                          setTimeout(function () {
                                              i.removeAttribute("data-kt-indicator"),
                                                  (i.disabled = !1),
                                                  function (t) {
                                                    t.isConfirmed && n.hide();
                                                }
                                          }, 2e3))
                                        : Swal.close();
                            });
                });
                    
                    
            })();
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTUsersAddUser.init();
});
